package br.com.imersao.JogoDado;

import java.util.Scanner;

public class Application {
	

	private static Scanner entrada1;
	private static Scanner entrada2;

	public static void main (String [] args){
		entrada1 = new Scanner(System.in);
		entrada2 = new Scanner(System.in);
			
		System.out.println("Digite qtde de faces que o dado deve ter");
		int face = entrada1.nextInt();
		System.out.println("Digite qtde de grupos");
		int grupo = entrada2.nextInt();
				
		JogaDado jogo = new JogaDado();
		jogo.Sorteio(face, grupo);		
		
	}



}
