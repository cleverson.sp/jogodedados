package br.com.imersao.JogoDado;

import java.util.Random;

public class JogaDado {
	
	
	public int ExecutaDado(int face) {
		
		Random numero = new Random();
		int valor = numero.nextInt(face)+1;

		return valor;		
	}

	public void Sorteio (int face, int grupo) {
		
		System.out.println("metodo sorteio");
		
		int[] vetor = new int [3];
		int soma=0;
		
		for (int i=1; i<=grupo;i++) {
			
			System.out.println("\nGrupo: "+i);
		
			for (int j=0; j<=2;j++) {
				vetor[j]=ExecutaDado(face);
				System.out.print(vetor[j] + ";");
				soma = soma+vetor[j];
			}
			
			System.out.println(" "+soma);
			soma =0;
			
		}
	
	}

	
}
